import {
  Mint as MintEvent,
  Burn as BurnEvent,
  LogSetAuthority as LogSetAuthorityEvent,
  LogSetOwner as LogSetOwnerEvent,
  LogNote as LogNoteEvent,
  Approval as ApprovalEvent,
  Transfer as TransferEvent
} from "../generated/DAI/Contract";
import {
  Mint,
  Burn,
  LogSetAuthority,
  LogSetOwner,
  LogNote,
  Approval,
  Transfer,
  Balance
} from "../generated/schema";
import { BigInt } from "@graphprotocol/graph-ts";

export function handleMint(event: MintEvent): void {
  let entity = new Mint(
    event.transaction.hash.toHex() + "-" + event.logIndex.toString()
  );
  entity.timestamp = event.block.timestamp;
  entity.guy = event.params.guy;
  entity.wad = event.params.wad;
  entity.save();
  handleMintBalance(event);
}

export function handleBurn(event: BurnEvent): void {
  let entity = new Burn(
    event.transaction.hash.toHex() + "-" + event.logIndex.toString()
  );
  entity.timestamp = event.block.timestamp;
  entity.guy = event.params.guy;
  entity.wad = event.params.wad;
  entity.save();
  handleBurnBalance(event);
}

export function handleLogSetAuthority(event: LogSetAuthorityEvent): void {
  let entity = new LogSetAuthority(
    event.transaction.hash.toHex() + "-" + event.logIndex.toString()
  );
  entity.timestamp = event.block.timestamp;
  entity.authority = event.params.authority;
  entity.save();
}

export function handleLogSetOwner(event: LogSetOwnerEvent): void {
  let entity = new LogSetOwner(
    event.transaction.hash.toHex() + "-" + event.logIndex.toString()
  );
  entity.timestamp = event.block.timestamp;
  entity.owner = event.params.owner;
  entity.save();
}

export function handleLogNote(event: LogNoteEvent): void {
  let entity = new LogNote(
    event.transaction.hash.toHex() + "-" + event.logIndex.toString()
  );
  entity.timestamp = event.block.timestamp;
  entity.sig = event.params.sig;
  entity.guy = event.params.guy;
  entity.foo = event.params.foo;
  entity.bar = event.params.bar;
  entity.wad = event.params.wad;
  entity.fax = event.params.fax;
  entity.save();
}

export function handleApproval(event: ApprovalEvent): void {
  let entity = new Approval(
    event.transaction.hash.toHex() + "-" + event.logIndex.toString()
  );
  entity.timestamp = event.block.timestamp;
  entity.src = event.params.src;
  entity.guy = event.params.guy;
  entity.wad = event.params.wad;
  entity.save();
}

export function handleTransfer(event: TransferEvent): void {
  let entity = new Transfer(
    event.transaction.hash.toHex() + "-" + event.logIndex.toString()
  );
  entity.timestamp = event.block.timestamp;
  entity.src = event.params.src;
  entity.dst = event.params.dst;
  entity.wad = event.params.wad;
  entity.save();
  handleTransferFromBalance(event);
  handleTransferToBalance(event);
}

function handleTransferFromBalance(event: TransferEvent): void {
  let entity = Balance.load(event.params.src.toHex());
  if (entity == null) {
    entity = new Balance(event.params.src.toHex());
    entity.address = event.params.src;
    entity.wad = BigInt.fromI32(0);
  }
  entity.wad = entity.wad.minus(event.params.wad);
  entity.save();
}

function handleTransferToBalance(event: TransferEvent): void {
  let entity = Balance.load(event.params.dst.toHex());
  if (entity == null) {
    entity = new Balance(event.params.dst.toHex());
    entity.address = event.params.dst;
    entity.wad = BigInt.fromI32(0);
  }
  entity.wad = entity.wad.plus(event.params.wad);
  entity.save();
}

function handleBurnBalance(event: BurnEvent): void {
  let entity = Balance.load(event.params.guy.toHex());
  if (entity == null) {
    entity = new Balance(event.params.guy.toHex());
    entity.address = event.params.guy;
    entity.wad = BigInt.fromI32(0);
  }
  entity.wad = entity.wad.minus(event.params.wad);
  entity.save();
}

function handleMintBalance(event: MintEvent): void {
  let entity = Balance.load(event.params.guy.toHex());
  if (entity == null) {
    entity = new Balance(event.params.guy.toHex());
    entity.address = event.params.guy;
    entity.wad = BigInt.fromI32(0);
  }
  entity.wad = entity.wad.plus(event.params.wad);
  entity.save();
}
